<?php

/* 
 * The MIT License
 *
 * Copyright 2019 Rodos.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace User;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

use Zend\Stdlib\ArrayObject;

class Module implements ConfigProviderInterface {

  public function getConfig() {
    return include __DIR__ . '/../config/module.config.php';
  }
  
/**
  * Set factories for our model.
  * 
  * @return array of factories.
  */
 public function getServiceConfig() {
    return [
        'factories' => [
            Model\UserTable::class => function($container) {
              $tableGateway = $container->get(Model\UserTableGateway::class);
              return new Model\UserTable($tableGateway);
            },
            Model\UserTableGateway::class => function ($container) {
              $dbAdapter = $container->get(AdapterInterface::class);
              $resultSetPrototype = new ResultSet();
              $resultSetPrototype->setArrayObjectPrototype(new Model\User());
              return new TableGateway('users', $dbAdapter, null, $resultSetPrototype);
            },
            Model\ResetPasswordTable::class => function($container) {
              $tableGateway = $container->get(Model\ResetPasswordTableGateway::class);
              return new Model\ResetPasswordTable($tableGateway);
            },
            Model\ResetPasswordTableGateway::class => function ($container) {
              $dbAdapter = $container->get(AdapterInterface::class);
              $resultSetPrototype = new ResultSet();
              $resultSetPrototype->setArrayObjectPrototype(new ArrayObject());
              return new TableGateway('reset_password_tokens', $dbAdapter, null, $resultSetPrototype);
            },
            Model\UserManager::class => function ($container) {
              $sessionContainer = new \Zend\Session\Container('user');
              return new Model\UserManager(
                      $container->get(Model\UserTable::class),
                      $container->get(Model\ResetPasswordTable::class),
                      $sessionContainer,
                      $container->get(Model\UserDB::class)
              );
            },
            Model\UserDB::class => function ($container) {
              $dbAdapter = $container->get(\Zend\Db\Adapter\Adapter::class);
              return new Model\UserDB($dbAdapter);
            },
        ],
    ];
  }

  /**
  * Set factories for our controllers.
  * 
  * @return array of factories.
  */
  public function getControllerConfig() {
     return [
         'factories' => [
             Controller\UserController::class => function($container) {
               return new Controller\UserController(
                       $container->get(Model\UserManager::class)
               );
             },
         ],
     ];
   }

}
