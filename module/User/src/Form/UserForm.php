<?php

/*
 * The MIT License
 *
 * Copyright 2019 Rodos.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element\Captcha;

/**
 * Description of UserForm
 *
 * @author Rodos
 */
class UserForm extends Form {

  public const USERNAME_PARAMS = [
      'name' => 'username',
      'type' => 'text',
      'options' => [
          'label' => 'Username',
      ]
  ];
  
  public const PASSWORD_PARAMS = [
      'name' => 'password',
      'type' => 'password',
      'options' => [
          'label' => 'Password',
      ],
  ];
  
  public const EMAIL_PARAMS = [
      'name' => 'email',
      'type' => 'email',
      'options' => [
          'label' => 'Email',
      ],
  ];
  
  public const FULL_NAME_PARAMS = [
      'name' => 'full_name',
      'type' => 'text',
      'options' => [
          'label' => 'Full name',
      ],
  ];
  
  public const LOCATION_PARAMS = [
      'name' => 'location',
      'type' => 'text',
      'options' => [
          'label' => 'Location',
      ],
  ];

  public const BIO_PARAMS = [
        'name' => 'bio',
        'type' => 'textarea',
        'options' => [
            'label' => 'Bio',
        ],
    ];
  
  public function __construct($name = 'user') {
    // We will ignore the name provided to the constructor
    parent::__construct($name);
    if ($name === 'user') {
      $this->add(self::USERNAME_PARAMS);
      $this->add(self::PASSWORD_PARAMS);
      $this->add([
          'name' => 'id',
          'type' => 'hidden',
      ]);
      $this->add(self::EMAIL_PARAMS);
      $this->add(self::FULL_NAME_PARAMS);
      $this->add(self::LOCATION_PARAMS);
      $this->add(self::BIO_PARAMS);
      
      $imgCapcha = new \Zend\Captcha\Image('img');
      $imgCapcha->setExpiration(60 * 20);
      $imgCapcha->setFont('./public/fonts/PTSans-Regular.ttf');

      $captcha = new Captcha('captcha');
      $captcha->setCaptcha($imgCapcha);
      $captcha->setLabel('Please verify you are human');
      $this->add($captcha);

      $this->add([
          'name' => 'submit',
          'type' => 'submit',
          'attributes' => [
              'value' => 'Go',
              'id' => 'submitbutton',
          ],
      ]);
    }
  }
}
