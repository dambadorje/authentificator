<?php

/*
 * The MIT License
 *
 * Copyright 2019 Rodos.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace User\Form;


use Zend\Form\Element\Captcha;
use User\Form\UserForm;

/**
 * Description of NewPasswordForm
 *
 * @author Rodos
 */
class NewPasswordForm extends UserForm {

  public function __construct($name = 'new_password') {
    parent::__construct($name);
    if ($name === 'new_password') {
      $this->add(UserForm::USERNAME_PARAMS);

      $imgCapcha = new \Zend\Captcha\Image('img');
      $imgCapcha->setExpiration(60 * 20);
      $imgCapcha->setFont('./public/fonts/PTSans-Regular.ttf');

      $captcha = new Captcha('captcha');
      $captcha->setCaptcha($imgCapcha);
      $captcha->setLabel('Please verify you are human');
      $this->add($captcha);

      $this->add([
          'name' => 'submit',
          'type' => 'submit',
          'attributes' => [
              'value' => 'Reset password',
              'id' => 'submitbutton',
          ],
      ]);
    }
  }
}
