<?php

/* 
 * The MIT License
 *
 * Copyright 2019 Rodos.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace User\Controller;


use Zend\InputFilter\InputFilter;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Validator\StringLength;


use User\Model\User;
use User\Model\UserManager;
use User\Form\UserForm;
use User\Form\ProfileForm;
use User\Form\LoginForm;
use User\Form\NewPasswordForm;
use User\Form\NewPasswordConfirmationForm;

/**
 * ActionController implementation for users. Connect actions with database
 *
 * @author Rodion Riabinin
 */
class UserController extends AbstractActionController {
  /**
   *
   * @var UserManager
   */
  private $userManager;
  /**
   * Filter for tokens InputFilter
   */
  public const TOKEN_FILTER = [
      'name' => 'token',
      'required' => true,
      'filters' => [
          ['name' => StripTags::class],
          ['name' => StringTrim::class],
      ],
      'validators' => [
          [
              'name' => StringLength::class,
              'options' => [
                  'encoding' => 'UTF-8',
                  'min' => 32,
                  'max' => 32,
              ],
          ],
          [
              'name' => 'Regex',
              'options' => ['pattern' => '/[\w]/'],
          ],
      ],
  ];

  public function __construct(UserManager $userManager) {
    $this->userManager = $userManager;
    
  }
  
  /** Filters the username from AJAX request
   * 
   * @param string $name
   * @return array Array with errors
   */
  public function getNameErrors(string $name) {
    $filter = new InputFilter();
    $filter->add(User::USERNAME_FILTER);
    $filter->setData(['username' => $name]);
    $jsonData = '';
    if (!$filter->isValid()) {
      $msg = [$filter->getMessages()];
      $jsonData = ['username' => $msg[0]['username']];
    } else {
      $exist = $this->userManager->isUserWithNameExist($name);
      if ($exist) {
        $jsonData = ['username' => ['' => 'User with username: "' . $name
                . '" already registered. ' . 'Try another.',
            ]
        ];
      }
    }
    return $jsonData;
  }
  
  /** Filters the email from AJAX request
   * 
   * @param string $email
   * @return array Array with errors
   */
  public function getEmailErrors(string $email) {
    $filter = new InputFilter();
    $filter->add(User::EMAIL_FILTER);
    $filter->setData(['email' => $email]);
    $jsonData = ['email' => ''];
    if (!$filter->isValid()) {
      $jsonData = ['email' => ['' => 'Email is not valid.']];
    } else {
      $exist = $this->userManager->isUserWithEmailExist($email);
      if ($exist) {
        $jsonData = ['email' => ['' => 'User with email: "' . $email
                . '" already registered. ' . 'Try another.',
            ]
        ];
      }
    }
    return $jsonData;
  }
/** Process AJAX requests
 * 
 * @return JsonModel
 */
  public function ajaxCheckAction() {
    $request = $this->getRequest();
    $name = $request->getPost()->name;
    $value = $request->getPost()->value;
    
    switch ($name) {
      case 'username':
        $jsonData = $this->getNameErrors($value);
        break;
      case 'email':
        $jsonData = $this->getEmailErrors($value);
        break;
   }
    $view = new JsonModel($jsonData);
    return $view;
  }
  /** Process new password confirmation tokens
   * 
   * @return type
   */
  public function newPasswordConfirmationAction() {
    $request = $this->getRequest();
    $token = $request->getQuery()->token;
    
    if (!$request->isPost()) {
      // Redirect if token not valid or not present in DB
    if (!self::isTokenValid($token) || !$this->userManager->getUserIdByResetPasswordToken($token)) {
      $this->flashMessenger()->addErrorMessage('This link expire or not exist.');
      return $this->redirect()->toRoute('user');
    }    
         
    // Show form
    $form = new NewPasswordConfirmationForm();
    $form->get('token')->setValue($token);
    //$request = $this->getRequest();
     
    
      return ['form' => $form];
    }

    // Validate form
    $filter = new InputFilter();
    $filter->add(self::TOKEN_FILTER)
          ->add(User::PASSWORD_FILTER)
            ->add(User::STRING_FILTER, 'submit');
    $filter->setData($request->getPost());
    
    if (!$filter->isValid()) {
      $this->flashMessenger()->addErrorMessage('This link expire or not exist.');
      return ['form' => $form];
    }
  
    
    try {
        // Set new password if submited
        if ($filter->getValue('submit')) {
          $this->userManager->processNewPasswordToken(
                  $filter->getValue('token'),
                  $filter->getValue('password'));
          $this->flashMessenger()->addSuccessMessage("New password confirmed. You may use it for login");
          return $this->redirect()->toRoute('user', ['action' => 'login']);
        }
        $this->flashMessenger()->addErrorMessage('This link expire or not exist.');
        return $this->redirect()->toRoute('user');
      
    } catch (\Exception $e) {
      $this->flashMessenger()->addErrorMessage('This link expire or not exist.');
    }

    return $this->redirect()->toRoute('user');
  }

  /** Show new password form and process it if submitted
   * 
   * @return type
   */
  public function newPasswordAction() {
    $this->updateLayout();
    $form = new NewPasswordForm();
    $request = $this->getRequest();

    if (!$request->isPost()) {
      return ['form' => $form];
    }

    $filter = new InputFilter();
    $filter->add(User::USERNAME_FILTER);
    $form->setInputFilter($filter);
    $form->setData($request->getPost());

    if (!$form->isValid()) {
      return ['form' => $form];
    }
    $formData = $form->getData();

    if ($this->userManager->newPassword($formData['username'])) {
      $this->flashMessenger()->addSuccessMessage("Check your email for reset password.");
    } else {
      $this->flashMessenger()->addErrorMessage(
              sprintf("User with username %s does not found.", $formData['username']));
    }
    return $this->redirect()->toRoute('user', ['action' => 'index']);
  }
  
/** Update user logged status in layout
 * 
 * @return void
 */
  public function updateLayout() {
    if ($this->userManager->isUserLogedIn()) {
      $this->layout()->username = $this->userManager->getLogedUser()->username;
    } else {
      $this->layout()->username = '';
    }
    return;
  }

  /**
  * Prepare data we wish to represent in view.
  * 
  * @return ViewModel instance.
  */
  public function indexAction() {
    $this->updateLayout();
    return new ViewModel([
        'users' => $this->userManager->getAllUsers(),
    ]);
  }

  public function loginAction() {
    // Redirect if already logged
    if($this->userManager->isUserLogedIn()){
      $this->flashMessenger()->addErrorMessage("You already logged in");
      return $this->redirect()->toRoute('user', ['action' => 'index']);
    }
    
    $this->updateLayout();
    // Show login form
    $form = new LoginForm();
    $request = $this->getRequest();

    if (! $request->isPost()) {
        return ['form' => $form];
    }
    
    $user = new User();
    
    $filter = new InputFilter();
    $filter->add(User::USERNAME_FILTER)
            ->add(User::PASSWORD_FILTER);
    
    $form->setInputFilter($filter);
    $form->setData($request->getPost());

    if (!$form->isValid()) {
      return ['form' => $form];
    }
    $formData = $form->getData();
    
    if ($this->userManager->login($formData['username'], $formData['password'],
            $formData['remember'])) {
      return $this->redirect()->toRoute('user', ['action' => 'index']);
    }
    $this->flashMessenger()->addErrorMessage("Login and password don't match or"
            . " not registered.");
    return $this->redirect()->toRoute('user', ['action' => 'login']); 
  }
  
  /** Process user logout
   * 
   * @return void
   */
  public function logoutAction() {
    $this->userManager->logout();    
    return $this->redirect()->toRoute('user');
  }
  
  /** New user registration action
   * 
   * @return type
   */
  public function registerAction() {
    // Redirect if user already logged
    if ($this->userManager->isUserLogedIn()) {
      $this->flashMessenger()->addErrorMessage("You already logged in");
      return $this->redirect()->toRoute('user', ['action' => 'index']);
    }
    
    $this->updateLayout();
    
    // Show form
    $form = new UserForm();
    $form->get('submit')->setValue('Register');

    $request = $this->getRequest();

    if (!$request->isPost()) {
      return ['form' => $form];
    }

    $user = new User();

    // Validate form
    $filter = new InputFilter();
    $filter->add(User::USERNAME_FILTER)
            ->add(User::EMAIL_FILTER)
            ->add(User::PASSWORD_FILTER)
            ->add(User::STRING_FILTER, 'full_name');

    $form->setInputFilter($filter);
    $form->setData($request->getPost());

    if (!$form->isValid()) {
      return ['form' => $form];
    }
    
    // Check for email in database
    $formData = $form->getData();
    $email = $formData['email'];
    if ($this->userManager->isUserWithEmailExist($email)) {
      $emailElement = $form->get('email');
      $emailElement->setMessages(['emailRegistered' => 'This email already registered.']);
      return ['form' => $form];
    }
      
    // Process new user data
    $user->exchangeArray($formData);
    $user->location = '';
    $user->bio = '';
    $this->userManager->addUser($user);
    
    $this->flashMessenger()->addWarningMessage('The email address will be changed '
            . 'only after confirmation. Please check your email.');
    return $this->redirect()->toRoute('user');
  }
  
/** Validate tokens
 * 
 * @param string $token
 * @return bool
 */
  static function isTokenValid($token) {
    $filter = new InputFilter();
    $filter->add(self::TOKEN_FILTER);
    $filter->setData(['token' => $token]);
    return $filter->isValid();
  }

  /** 
   * Process new email tokens
   * Set new email to user if token match
   * @return type
   */
  public function newEmailConfirmationAction() {
    $this->updateLayout();
    $request = $this->getRequest();
    $token = $request->getQuery()->token;

    if (self::isTokenValid($token)) {
      try {
        $userId = $this->userManager->processNewEmailToken($token);
      } catch (\Exception $e) {
        $this->flashMessenger()->addErrorMessage('This link expire or not exist.');
        return $this->redirect()->toRoute('user');
      }
      if ($userId) {
        $this->userManager->setLoggedById($userId);
        $this->flashMessenger()->addSuccessMessage("Email confirmed.");
        return $this->redirect()->toRoute('user');
      }
    }
    $this->flashMessenger()->addErrorMessage('This link expire or not exist.');
    return $this->redirect()->toRoute('user');
  }

  /** Show and process user profile
   * 
   * @return UserForm
   */
  public function profileAction() {
    $this->updateLayout();
    if (!$this->userManager->isUserLogedIn()) {
      $this->flashMessenger()->addErrorMessage('You are not logged in please login to continue.');
      return $this->redirect()->toRoute('user', ['action' => 'login']);
    }

    // Redirect to index if failed get user from DB
    try {
      $user = $this->userManager->getLogedUser();
    } catch (\Exception $e) {
      return $this->redirect()->toRoute('user');
    }
    // store old email
    $oldEmail = $user->email;

    // Show form
    $form = new ProfileForm();
    $form->bind($user);

    $request = $this->getRequest();
    if (!$request->isPost()) {
      return ['form' => $form];
    }

    $data = $request->getPost();
    // Set data or validation fail
    $data->set('id', $user->id);
    $data->set('password', '12qwAS!@');
    
    // validate
    $form->setInputFilter($user->getInputFilter());
    $form->setData($data);
    $form->remove('password');
      
    if (!$form->isValid()) {
      return ['form' => $form];
    }
    
    // Delete profile if button pressed
    $del = $data->del;
    if ($del) {
      return $this->redirect()->toRoute('user', ['action' => 'delete']);
    }
    
    // Start email confirmation if changed
    if ($oldEmail !== $user->email) {
      $this->userManager->setNewEmail($user, $user->email);
      $this->flashMessenger()->addWarningMessage('The email address will be changed '
              . 'only after confirmation. Please check your email.');
    }
    
    // Update other user data
    $this->userManager->updateUserProfile($user);
    $this->flashMessenger()->addSuccessMessage("Profile data saved.");

    return $this->redirect()->toRoute('user');
  }
  
  /** Delete profile of logged user
   * 
   * @return void
   */
  public function deleteAction() {
    if (!$this->userManager->isUserLogedIn()) {
      $this->flashMessenger()->addErrorMessage('You are not logged in please login to continue.');
      return $this->redirect()->toRoute('user', ['action' => 'login']);
    }

    $request = $this->getRequest();
    if (!$request->isPost()) {
      return ['username' => 'CurUser'];
    }

    $del = $request->getPost('del', 'No');

    if ($del == 'Yes') {
      $this->userManager->deleteLoggedUser();
      $this->flashMessenger()->addSuccessMessage("Profile delited.");
      $this->userManager->logout();
    }
    return $this->redirect()->toRoute('user');
  }

}
