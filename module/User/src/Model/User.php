<?php

/*
 * The MIT License
 *
 * Copyright 2019 Rodos.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace User\Model;

use DomainException;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Filter\ToInt;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\StringLength;
use Zend\Validator\EmailAddress;

/**
 * Represents user data
 * 
 * 
 * @param array of user data
 * @return void
 */
class User implements InputFilterAwareInterface {
  
/** User id
 *
 * @var int 
 */
  public $id;
  
  /** Username
   *
   * @var string
   */
  public $username;
  
  /** User password
   *
   * @var string
   */
  public $password;
  
  /** User email address
   *
   * @var string 
   */
  public $email;
  
  /** User full name
   *
   * @var string
   */
  public $full_name;
  
  /** Information where the user is from.
   *
   * @var string
   */
  public $location;
  
  /** User biography
   *
   * @var string
   */
  public $bio;
  
  /** Password expire token
   *
   * @var string
   */
  public $pwd_expire;

  /** Input filters for all user properties
   *
   * @var InputFilter
   */
  private $inputFilter;
  
  /**
   *  Data validators for user id
   */
  public const ID_FILTER = [
      'name' => 'id',
      'required' => true,
      'filters' => [['name' => ToInt::class],],
  ];
  
  /**
   *  Data validators for username
   */
  public const USERNAME_FILTER = [
      'name' => 'username',
      'required' => true,
      'filters' => [
          ['name' => StripTags::class],
          ['name' => StringTrim::class],
      ],
      'validators' => [
          [
              'name' => StringLength::class,
              'options' => [
                  'encoding' => 'UTF-8',
                  'min' => 4,
                  'max' => 100,
              ],
          ],
          [
              'name' => 'Regex',
              'options' => [
                  'pattern' => '/^(?![_.-])(?!.*[_.-]{2})[a-zA-Z0-9._-]+(?<![_.-])$/',
                  'messages' => [
                      'regexNotMatch' => 'The username may contain only latin characters separated by "._-"',
                  ],
                  
              ],
          ]
      ],
  ];
  
  /**
   *  Data validators for password
   */
  public const PASSWORD_FILTER = [
      'name' => 'password',
      'required' => true,
      'filters' => [
          ['name' => StripTags::class],
          ['name' => StringTrim::class],
      ],
      'validators' => [
          [
              'name' => StringLength::class,
              'options' => [
                  'encoding' => 'UTF-8',
                  'min' => 8,
                  'max' => 100,
              ],
              'break_chain_on_failure' => true,
          ],
          [
              'name' => 'Regex',
              'options' => [
                  'pattern' => '/[a-z]/',
                  'messages' => [
                      'regexNotMatch' => 'Password must contain at least one lowercase letter',
                  ],
              ],
          ],
          [
              'name' => 'Regex',
              'options' => [
                  'pattern' => '/[A-Z]/',
                  'messages' => [
                      'regexNotMatch' => 'Password must contain at least one capital letter',
                  ],
              ],
          ],
          [
              'name' => 'Regex',
              'options' => [
                  'pattern' => '/[0-9]/',
                  'messages' => [
                      'regexNotMatch' => 'Password must contain at least one digit',
                  ],
              ],
          ],
          [
              'name' => 'Regex',
              'options' => [
                  'pattern' => '/[.!@#$%^&*;:]/',
                  'messages' => [
                      'regexNotMatch' => 'Password must contain at least one of this symbols [.!@#$%^&*;:]',
                  ],
              ],
          ],
      ],
  ];
  
  /**
   *  Data validators for email
   */
  public const EMAIL_FILTER = [
      'name' => 'email',
      'required' => true,
      'filters' => [
          ['name' => StripTags::class],
          ['name' => StringTrim::class],
      ],
      'validators' => [
          [
              'name' => StringLength::class,
              'options' => [
                  'encoding' => 'UTF-8',
                  'min' => 4,
                  'max' => 100,
              ],
          ],
          [
              'name' => EmailAddress::class,
              
          ],
      ],
  ];

  /**
   *  Data validators other strings
   */
  public const STRING_FILTER = [
      'required' => true,
      'filters' => [
          ['name' => StripTags::class],
          ['name' => StringTrim::class],
      ],
      'validators' => [
          [
              'name' => StringLength::class,
              'options' => [
                  'encoding' => 'UTF-8',
                  'min' => 4,
                  'max' => 100,
              ],
          ],
          [
              'name' => 'Regex',
              'options' => ['pattern' => '/[\w\s]/'],
          ],
      ],
  ];

  /**
   * Fill user data from an array.
   * 
   * @param array of user data
   * @return void
   */
  public function exchangeArray(array $data) {
    $this->id = !empty($data['id']) ? $data['id'] : null;
    $this->username = !empty($data['username']) ? $data['username'] : null;
    $this->password = !empty($data['password']) ? $data['password'] : null;
    $this->email = !empty($data['email']) ? $data['email'] : null;
    $this->full_name = !empty($data['full_name']) ? $data['full_name'] : null;
    $this->location = !empty($data['location']) ? $data['location'] : null;
    $this->bio = !empty($data['bio']) ? $data['bio'] : null;
    $this->pwd_expire = !empty($data['pwd_expire']) ? $data['pwd_expire'] : null;
  }

  /**
   * Return user data as array. Usually used by hydrators.
   * 
   * 
   * @return array of user data
   */
  public function getArrayCopy() {
    return [
        'id' => $this->id,
        'username' => $this->username,
        'password' => $this->password,
        'email' => $this->email,
        'full_name' => $this->full_name,
        'location' => $this->location,
        'bio' => $this->bio,
        'pwd_expire' => $this->pwd_expire,
    ];
  }

  public function setInputFilter(InputFilterInterface $inputFilter) {
    throw new DomainException(sprintf(
                    '%s does not allow injection of an alternate input filter',
                    __CLASS__
    ));
  }

  /**
   * Returns an InputFilter for user data.
   * 
   * @return InputFilter 
   */
  public function getInputFilter() {
    if ($this->inputFilter) {
      return $this->inputFilter;
    }

    $inputFilter = new InputFilter();

    $inputFilter->add(self::USERNAME_FILTER);
    $inputFilter->add(self::ID_FILTER);
    $inputFilter->add(self::PASSWORD_FILTER);
    $inputFilter->add(self::EMAIL_FILTER);
    $inputFilter->add(self::STRING_FILTER, 'full_name');
    $inputFilter->add(self::STRING_FILTER, 'location');
    $inputFilter->add(self::STRING_FILTER, 'bio');
    $this->inputFilter = $inputFilter;
    return $this->inputFilter;
  }

}
