<?php

/*
 * The MIT License
 *
 * Copyright 2019 Rodos.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace User\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Delete;
use DateTime;
use DateInterval;

/**
 *  Table of Reset Password tokens
 *
 * @author Rodos
 */
class ResetPasswordTable {
/**
 *
 * @var TableGatewayInterface 
 */
  private $tableGateway;

  public function __construct(TableGatewayInterface $tableGateway) {
    $this->tableGateway = $tableGateway;
  }

  /**
   *  Delete all expired tokens from table
   */
  public function deleteAllExpiredTokens() {
    $this->tableGateway->delete(function (Delete $delete) {
      $date = new DateTime();
      $delete->where->lessThan('expire_time', $date->format('Y-m-d H:i:s'));
    });
  }

  /** Delete row with token
   * 
   * @param string $token
   * @throws RuntimeException
   */
  public function deleteToken(string $token) {
    try {
      $this->tableGateway->delete(['token' => $token]);
    } catch (RuntimeException $e) {
      throw new RuntimeException(sprintf(
                      'Token: %d; does not exist.', $token
      ));
    }
  }

  /** Return user id linked to token
   * 
   * @param string $token
   * @return type
   * @throws RuntimeException
   */
  public function getUserIdByToken(string $token) {
    $rowset = $this->tableGateway->select(['token' => $token]);
    $row = $rowset->current();    
    return $row['user_id'];
  }

  /** Add token to table
   * 
   * @param int $userId
   * @param string $token
   * @return type
   */
  public function addToken(int $userId, string $token) {
    $date = new DateTime();
    $date->add(new DateInterval('P1D'));

    $this->tableGateway->insert([
        'token' => $token,
        'user_id' => $userId,
        'expire_time' => $date->format('Y-m-d H:i:s'),
    ]);
    return;
  }

}
