<?php

/*
 * The MIT License
 *
 * Copyright 2019 Rodos.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace User\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;

/**
 * Reprsent users table in database
 *
 * @author Rodion Riabinin
 */
class UserTable {

  private $tableGateway;
  
  // Salt for password encription
  const PRE_PASSWORD_SALT = 'qD&h*';
  const POST_PASSWORD_SALT = '-g!@';

  public function __construct(TableGatewayInterface $tableGateway) {
    $this->tableGateway = $tableGateway;
  }

  /**
   * Get all userdata from database.
   * 
   * @return array of all userdata from database.
   */
  public function fetchAll() {
    $where = new Where();
    $where->notEqualTo('email', '');
    return $this->tableGateway->select($where);
  }

  /**
   * Get user data from database by user id.
   * 
   * @param int $id User id.
   * @return array of userdata from database.
   * @throws RuntimeException 'Could not find row with identifier %d'
   */
  public function getUserById(int $id) {
    $rowset = $this->tableGateway->select(['id' => $id]);
    $row = $rowset->current();
    if (!$row) {
      throw new RuntimeException(sprintf(
                      'Could not find row with identifier %d',
                      $id
      ));
    }

    return $row;
  }

  /**
   * Get user id from database by username.
   * 
   * @param string $username
   * @return int User id.
   * @throws RuntimeException 'Could not find row with usename: %s'
   */
  public function gerUserIdByUsername(string $username) {
    $row = $this->getByName($username);
    if (!$row) {
      throw new RuntimeException(sprintf(
                      'Could not find row with usename: %s', $username
      ));
    }
    return $row->id;
  }

  /**
   * Check is login and password match.
   * 
   * @param string $username
   * @param string $password
   * @return bool True if login and password match otherwise returns false.
   */
  public function isPasswordMatch($username, $password) {
    $row = $this->getByName($username);
    if ($row) {
      if ($row->password === $this->encryptPassword($password)) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Get user data from DB by user name.
   * 
   * @param string $username
   * @return array of user data.
   */
  public function getByName(string $username) {
    $where = new Where();
    $where->equalTo('username', $username);
    $where->notEqualTo('email', '');
    $rowset = $this->tableGateway->select($where);
    $row = $rowset->current();
    return $row;      
  }

  /**
   * Check is user with name present in DB
   * 
   * @param string $username
   * @return bool True if user exist otherwise returns false.
   */
  public function isUserWithNameExist(string $username) {
    if ($this->getByName($username)) {
      return true;
    }
    return false;
  }
  
  /**
   * Check is user with email present in DB
   * 
   * @param string $email
   * @return bool True if user exist otherwise returns false.
   */
  public function isUserWithEmailExist(string $email) {
    $rowset = $this->tableGateway->select(['email' => $email]);
    $row = $rowset->current();
    if ($row) {
      return true;
    }
    return false;
  }
  /**
   * Writes user data to database. Insert new entry if user id equal to zero
   * and username not presen in DB
   * @param User $user User object.
   * @return void
   * @throws RuntimeException 'Cannot update user with username %s already exist'.
   * @throws RuntimeException 'Cannot update user with identifier %d; does not exist'.
   */
  public function addUser(User $user, string $newEmailToken) {
    // Check is already present in DB
    if ($this->isUserWithNameExist($user->username)) {
      throw new RuntimeException(sprintf(
                      'Cannot create user with username: %s already exist',
                      $user->username
      ));
    }
    if ($this->isUserWithEmailExist($user->email)) {
      throw new RuntimeException(sprintf(
                      'Cannot create user with email: %s already exist',
                      $user->email
      ));
    }
    
    $data = [
        'username' => $user->username,
        'password' => $this->encryptPassword($user->password),
        'email' => '',
        'new_email' => $user->email,
        'full_name' => $user->full_name,
        'location' => $user->location,
        'bio' => $user->bio,
        'pwd_expire' => $user->pwd_expire,
        'new_email_token' => $newEmailToken,
    ];
    
      $this->tableGateway->insert($data);
      return;    
  }  

  /**
   * Writes user data to database. Don't update: username, password, email
   * 
   * @param User $user User object.
   * @return void
   * @throws RuntimeException 'Cannot update user with identifier %d; does not exist'.
   */
  public function updateUserProfile(User $user) {
    $data = [
        'full_name' => $user->full_name,
        'location' => $user->location,
        'bio' => $user->bio
    ];

    try {
      $this->tableGateway->update($data, ['id' => $user->id]);
    } catch (RuntimeException $ex) {
      throw new RuntimeException(sprintf(
                      'Cannot update user with identifier %d; does not exist',
                      $user->id
      ));
    }
    
  }

  /**
   * Delete user from database by id.
   * 
   * @param int $id User id.
   * @return void
   * @throws RuntimeException 'Cannot delete user with identifier %d; does not exist'.
   */
  public function deleteUser($id) {
    try {
      $this->tableGateway->delete(['id' => (int) $id]);
    } catch (RuntimeException $e) {
      throw new RuntimeException(sprintf(
                      'Cannot delete user with identifier %d; does not exist',
                      $id
      ));
    }
  }
  
  public function encryptPassword(string $password) {
    $salted = self::PRE_PASSWORD_SALT . $password . self::POST_PASSWORD_SALT;
    return hash('ripemd128', $salted);
  }

  public function setNewEmail(int $userId, string $email, string $newEmailToken) {
    $data = [
        'new_email' => $email,
        'new_email_token' => $newEmailToken,
    ];

    try {
      $this->tableGateway->update($data, ['id' => $userId]);
    } catch (RuntimeException $ex) {
      throw new RuntimeException(sprintf(
                      'Cannot update user with identifier %d; does not exist',
                      $userId
      ));
    }   
  }
  
  /** If token match updates the email field with the value from new_email field 
   * 
   * @param string $newEmailToken
   * @return int User id attached to a token.
   * @throws RuntimeException 'Fail to process new email token'
   */
  public function processNewEmailToken(string $newEmailToken) {
    $id = $this->getUserIdByNewEmailToken($newEmailToken);
    
    $data = ['email' => new Expression('`new_email`'),
        'new_email' => '', 'new_email_token' => ''];
    $where = new Where();
    $where->equalTo('new_email_token', $newEmailToken);
    try {
    $results = $this->tableGateway->update($data, $where);
    } catch (RuntimeException $ex) {
      throw new RuntimeException('Fail to process new email token');
    }
    if ($results) {
      return $id;
    }
    return 0;
  }
  
  public function getUserIdByNewEmailToken(string $newEmailToken) {
    try {
      $rowset = $this->tableGateway->select(['new_email_token' => $newEmailToken]);
      $row = $rowset->current();      
    } catch (RuntimeException $ex) {
      throw new RuntimeException('Fail to process new email token');
    }
    return $row->id;
  }

}
