<?php

/*
 * The MIT License
 *
 * Copyright 2019 Rodos.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace User\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Exception\RuntimeException;

/**
 * Description of UserDB
 *
 * @author Rodos
 */
class UserDB {
/**
 *
 * @var Zend\Db\Adapter\Adapter
 */
  private $dbAdapter;
  /**
   *
   * @var Connection
   */
  private $connection;

  public function __construct(Adapter $dbAdapter) {
    $this->dbAdapter = $dbAdapter;
  }
  /**
   * Create connection and begin transaction
   */
  private function beginTransaction() {
    $this->connection = $this->dbAdapter->getDriver()->getConnection();
    $this->connection->beginTransaction();
  }
  
  /**
   * Rollback current transaction queries
   */
  private function rollback() {
    $this->connection->rollback();
  }
  
  /*
   * Commit current transaction queries
   */
  private function commit() {
    $this->connection->commit();
  }
  /**
   * Set new encrypted password. Update password expired token.
   * Delete reset password token.
   * @param string $token Reset password token
   * @param string $encPass Encrypted password
   * @param string $pwdExpire New expire password token
   * @return void
   * @throws RuntimeException 'DB fail to process new password token'
   */
  public function processNewPasswordToken(string $token, string $encPass, string $pwdExpire) {
    $updateSqlString = sprintf(
            'UPDATE `users`, `reset_password_tokens` 
             SET `users`.`password` = "%s",
             `users`.`pwd_expire` = "%s"
             WHERE `users`.`id` = `reset_password_tokens`.`user_id` AND
                `reset_password_tokens`.`token` = "%s"',
            $encPass, $pwdExpire, $token);
    
    $sql = new Sql($this->dbAdapter);
    $delete = $sql->delete('reset_password_tokens');
    $delete->where(['reset_password_tokens.token' => $token]);
    $deleteSqlString = $sql->buildSqlString($delete);
    
    $this->beginTransaction();
    try {
      $this->dbAdapter->query($updateSqlString, $this->dbAdapter::QUERY_MODE_EXECUTE);
      $this->dbAdapter->query($deleteSqlString, $this->dbAdapter::QUERY_MODE_EXECUTE);
      $this->commit();
    } catch (Exception $ex) {
      $this->rollback();
      throw new RuntimeException('DB fail to process new password token');
    }
    return;
  }
  //put your code here
}
