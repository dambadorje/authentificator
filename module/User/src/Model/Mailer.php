<?php

/*
 * The MIT License
 *
 * Copyright 2019 Rodos.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace User\Model;

use Zend\Mail;
use Zend\Mail\Transport\Sendmail;
use Zend\Mail\Address;


/**
 * Collect email functions
 *
 * @author Rodos
 */


class Mailer {
  /**
   *
   * @var TransportInterface 
   */
  public $transport;
  
  /**
   *
   * @var Address
   */
  public $fromAddress;
  
  /**
   *
   * @var Address
   */
  public $adminAddress;
          
  function __construct() {
    $this->transport = new Sendmail();
    $this->fromAddress = new Address('noreply@authentificator.ho.ua', '');
    $this->adminAddress = new Address('rodionrabinin@gmail.com', 'Rodio Riabinin');
  }

/** Send reset password confirmation email
 * 
 * @param string $email User email
 * @param string $fullName User full name
 * @param string $url Site url
 * @param string $token Used for create confirmation link
 */
  public function sendResetPasswordConfirmaion(string $email, string $fullName,
          string $url, string $token) {
    $mail = new Mail\Message();
    
    $h = new Mail\Headers();
    $h->addHeaderLine('Content-Type', 'text/html');
    $mail->setEncoding('UTF-8')
            ->setHeaders($h);
    $mail->setTo($email, $fullName)
         ->addFrom($this->fromAddress);
    $mail->setBody('<p>' . $fullName . ", someone request a password change "
            . "on the site: ". $url . " If it is not you, don't click on "
            . 'link below.</p><p><a href="http://'
            . $url . '/user/new_password_confirmation?token='
            . $token . '&">Reset password</a></p>');
    $mail->setSubject('Password reset from ' . $url);
    
    $this->transport->send($mail);
  }
/** Send email address confirmation email
 * 
 * @param string $email User email
 * @param string $fullName User full name
 * @param string $url Site url
 * @param string $token Used for create confirmation link
 */
  public function sendEmailConfirmaion(string $email, string $fullName,
          string $url, string $token) {
    $mail = new Mail\Message();
    
    $h = new Mail\Headers();
    $h->addHeaderLine('Content-Type', 'text/html');
    $mail->setEncoding('UTF-8')
            ->setHeaders($h);
    $mail->setTo($email, $fullName)
         ->addFrom($this->fromAddress);
    $mail->setBody('<p>' . $fullName . ", this email address was listed "
            . "at the site: ". $url . " If it is not you, don't click on "
            . 'link below.</p><p><a href="http://'
            . $url . '/user/new_email_confirmation?token='
            . $token . '&">Confirm email</a></p>');
    $mail->setSubject('Email confirmation from ' . $url);
    
    $this->transport->send($mail);
  }
}
