<?php

/*
 * The MIT License
 *
 * Copyright 2019 Rodos.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace User\Model;

use Zend\Session\Container;
use Zend\View\Helper\ServerUrl;
use Zend\Math\Rand;

use User\Model\User;
use User\Model\UserTable;
use User\Model\ResetPasswordTable;
use User\Model\Mailer;
use User\Model\UserDB;

/**
 * Description of UserManager
 *
 * @author Rodos
 */
class UserManager {
   /**
   *  @var UserTable Table with registered user data
   */
  private $usersTable;
  
  /**
   *  @var ResetPasswordTable Table with reset password tokens
   */
  private $resetPasswordTable;
  
  /**
   * @var Container Session data wrapper
   */
  private $sessionContainer;
  
  /** For sending emails to user
   * @var Mailer $mailer
   */
  private $mailer;

  private $db;
  /** Constructor
   * 
   * @param UserTable $userTable
   * @param ResetPasswordTable $resetPasswordTable
   * @param Container $sessionContainer
   */
  public function __construct(UserTable $userTable, ResetPasswordTable $resetPasswordTable,
          Container $sessionContainer, UserDB $db) {
    $this->usersTable = $userTable;
    $this->resetPasswordTable = $resetPasswordTable;    
    $this->sessionContainer = $sessionContainer;
    $this->mailer = new \User\Model\Mailer();
    $this->db = $db;
  }
  
  /** 
   * 
   * @return boolean Returns true if password of logged user is changed
   */
  public function isPasswordChanged() {
    $user = $this->getLogedUser();
    return !($this->sessionContainer->pwd_expire === $user->pwd_expire);
  }

  /**
   * 
   * @return boolean Returns true if user logged in
   */
  public function isUserLogedIn() {
    // Get user id from session
    $id = $this->sessionContainer->id;

    if ($id > 0) {
      if ($this->isPasswordChanged()) {
        $this->logout();
        return false;
      }
      return true;
    }
    return false;
  }

  /** Login as user
   * 
   * @param string $username User login
   * @param string $password User password
   * @param boolean $rememberMe Set session expiration time to 48h if chcked
   * @return boolean Return false if login and password do not match
   */
  public function login(string $username, string $password, bool $rememberMe) {
    if ($this->usersTable->isPasswordMatch($username, $password)) {
      $this->setLoggedAs($username);
      // Set session expiration time
      if ($rememberMe) {
        $this->sessionContainer->setExpirationSeconds(60 * 60 * 48);
      }
      return true;
    }
    return false;
  }

  /** Logout current session
   * 
   * @return void
   */
  public function logout() {
    // Clear user data in session
    $this->sessionContainer->setExpirationSeconds(-1);
    return;
  }
  
  /** Add user to DB and send confirmation email
   * 
   * @param User $user Object with user data
   */
  public function addUser(User $user) {
    $user->pwd_expire = $this->generateToken(8);
    $newEmailToken = $this->generateToken(32);
    $this->usersTable->addUser($user, $newEmailToken);
    $server = new ServerUrl();
    $url = $server->getHost();
    $this->mailer->sendEmailConfirmaion($user->email, $user->full_name, $url, $newEmailToken);
  }
  
  /** Updates full name, location and bio in user data table
   *  Do not update other fields
   * 
   * @param User $user Object with user data
   */
  public function updateUserProfile(User $user) {
    $this->usersTable->updateUserProfile($user);
  }
  
  /**
   * Returns logged user id or zero if not logged
   * @return int Session id or zero if not logged
   */
  public function getLogedUserId() {
    $this->isPasswordChanged();
    return (int)$this->sessionContainer->id;
  }
  
  /**
   * Returns array of user data
   * @return array Array of user data
   */
  public function getLogedUser() {
    return $this->usersTable->getUserById($this->sessionContainer->id);
  }
  
  /**
   * Returns array of users data
   * @return array Array of users data
   */
  public function getAllUsers() {
    return $this->usersTable->fetchAll();
  }
  /**
   * Generates a alphanumeric token of the specified length.
   * @param int $length Length of token
   * @return string Alphanumeric token
   */
  public function generateToken(int $length) {
    return Rand::getString($length, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                    . 'abcdefghijklmnopqrstuvwxyz0123456789');
  }

  /** Generate new password token and send email with confirmation link to user
   * 
   * @param string $username
   * @return boolean Return true on success otherwise false
   */
  public function newPassword(string $username) {
    $dbData = $this->usersTable->getByName($username);
    if ($dbData) {
      $server = new ServerUrl();
      $url = $server->getHost();

      // Delete Expired tokens
      $this->resetPasswordTable->deleteAllExpiredTokens();

      // Generate token
      $tokenString = $this->generateToken(32);

      // Add token to DB
      $this->resetPasswordTable->addToken($dbData->id, $tokenString);

      // Send email to user
      $this->mailer->sendResetPasswordConfirmaion($dbData->email, 
              $dbData->full_name, $url, $tokenString);
      return true;
    }
    return false;
  }
  /** Returns id associated with password token
   * 
   * @param string $token
   * @return int User id
   * @return null If find nothing
   */
  public function getUserIdByResetPasswordToken(string $token) {
    $this->resetPasswordTable->deleteAllExpiredTokens();
    return $this->resetPasswordTable->getUserIdByToken($token);
  }  
  
  /** 
   *  Take username and set user with this name as logged in
   * @param string $username
   * @return void
   */
  public function setLoggedAs(string $username) {
    $id = $this->usersTable->gerUserIdByUsername($username);
    $user = $this->usersTable->getUserById($id);
    
    // Set session expiration data
    $this->sessionContainer->id = $id;
    $this->sessionContainer->username = $user->username;
    $this->sessionContainer->pwd_expire = $user->pwd_expire;
    return;
  }
  
  /**
   * Take user id and set user with this id as logged in
   * @param int $userId
   * @return type
   */
  public function setLoggedById(int $userId) {
    $user = $this->usersTable->getUserById($userId);
    
    // Set session expiration data
    $this->sessionContainer->id = $userId;
    $this->sessionContainer->username = $user->username;
    $this->sessionContainer->pwd_expire = $user->pwd_expire;
    return;
  }
  
  /**
   * Returns true if user with name exist
   * @param string $username
   * @return boolean
   */
  public function isUserWithNameExist(string $username) {
    return $this->usersTable->isUserWithNameExist($username);
  }
  
  /**
   * Returns true if user with email exist
   * @param string $email
   * @return boolean
   */
  public function isUserWithEmailExist(string $email) {
    return $this->usersTable->isUserWithEmailExist($email);
  }
  /**
   * Set new unconfirmed email address and send email confirmation letter
   * @param User $user
   * @param string $email
   */
  public function setNewEmail(User $user, string $email) {
    $newEmailToken = $this->generateToken(32);
    $server = new ServerUrl();
    $url = $server->getHost();
    $this->usersTable->setNewEmail($user->id, $email, $newEmailToken);
    $this->mailer->sendEmailConfirmaion($email, $user->full_name,
            $url, $newEmailToken);
  }
  
  /** If token match updates the email field with the value from new_email field
   * 
   * @param string $token
   * @return boolean Returns true on success otherwise false
   */
  public function processNewEmailToken(string $token) {
    return $this->usersTable->processNewEmailToken($token);
  }
  
  /**
   * Delete currently logged user
   * @return void
   */
  public function deleteLoggedUser() {
    $this->usersTable->deleteUser($this->getLogedUserId());
    return;
  }
  
  /**
   * Find user by token and set new password
   * @param string $token
   * @param string $password
   * @return type
   */
  public function processNewPasswordToken(string $token, string $password) {
    $this->resetPasswordTable->deleteAllExpiredTokens();
    $encPass = UserTable::encryptPassword($password);
    $pwdExpire = $this->generateToken(8);
    $this->db->processNewPasswordToken($token, $encPass, $pwdExpire);
    return;
  }
  //put your code here
}
