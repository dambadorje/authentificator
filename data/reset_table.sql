CREATE TABLE IF NOT EXISTS reset_password_tokens (
    token CHAR(32) PRIMARY KEY NOT NULL,
    user_id INT,
    CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) 
        REFERENCES `users` (`id`) ON DELETE CASCADE,
    expire_time DATETIME NOT NULL
);