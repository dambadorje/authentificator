CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    username varchar(100) NOT NULL,
    password varchar(100) NOT NULL,
    email varchar(100) NOT NULL,
    full_name varchar(100) NOT NULL,
    location varchar(100) NOT NULL,
    bio varchar(100) NOT NULL,
    pwd_expire char(8)
);

CREATE TABLE reset_password_tokens (
    token CHAR(32) PRIMARY KEY NOT NULL,
    user_id INT,
    CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) 
        REFERENCES `users` (`id`) ON DELETE CASCADE,
    expire_time DATETIME NOT NULL
);

INSERT INTO users (username, password, email, full_name, location, bio)
VALUE ('Rodos', '123244566546', 'dambadorje@ukr.net', 'Rodion Riabinin', 'Krivoi Rog', 'No');